import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_alias(hub, ctx, aws_kms_key):
    # Create alias
    name = "alias/idem-fixture-alias-" + str(uuid.uuid4())
    target_key_id = aws_kms_key.get("resource_id")
    ret = await hub.states.aws.kms.alias.present(
        ctx, name=name, target_key_id=target_key_id
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")

    resource = ret.get("new_state")
    resource_id = resource.get("resource_id", None)
    assert name == resource_id

    # Verify that describe output format is correct
    describe_ret = await hub.states.aws.kms.alias.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.kms.alias.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.kms.alias.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert "arn" in described_resource_map and "target_key_id" in described_resource_map
    assert target_key_id == described_resource_map.get("target_key_id")

    # Create a new key to test update
    key_name = "idem-fixture-kms-key-new" + str(uuid.uuid4())
    ret = await hub.states.aws.kms.key.present(ctx, key_name)

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_kms_key = ret.get("new_state")

    # Update alias
    ret = await hub.states.aws.kms.alias.present(
        ctx, name=name, target_key_id=new_kms_key.get("resource_id")
    )
    assert ret["result"], ret["comment"]
    assert (
        ret["comment"]
        == f"Updated alias of KMS key {new_kms_key.get('resource_id')} to {name}"
    )

    # updating without any changes should not make a call to AWS and simply return a message no changes.
    ret = await hub.states.aws.kms.alias.present(
        ctx, name=name, target_key_id=new_kms_key.get("resource_id")
    )
    assert ret["result"], ret["comment"]
    assert (
        ret["comment"]
        == f"KMS Alias '{name}' is already associated with the KMS target key '{new_kms_key.get('resource_id')}'"
    )

    # If we pass an invalid alias name like without prefix alias/ or any invalid alias name
    name = "idem-fixture-alias-" + str(uuid.uuid4())
    ret = await hub.states.aws.kms.alias.present(
        ctx, name=name, target_key_id=target_key_id
    )
    assert not ret["result"], ret["comment"]
    assert "(ValidationException)" in ret["comment"][0]

    # Delete alias
    ret = await hub.states.aws.kms.alias.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert ret["comment"] == f"aws.kms.alias '{resource_id}' is deleted"

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.kms.alias.absent(ctx, name=resource_id)
    assert ret["comment"] == f"aws.kms.policy '{resource_id}' already absent"
