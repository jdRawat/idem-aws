import uuid

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_organization_policy(hub, ctx, aws_organization):
    policy_name = "idem-test-organizations-policy-" + str(uuid.uuid4())
    description = "Enables admins of attached accounts to delegate all S3 permissions"
    policy_type = "SERVICE_CONTROL_POLICY"
    content = """{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:DeleteBucket",
        "s3:DeleteObject",
        "s3:DeleteObjectVersion"
      ],
      "Resource": "*",
      "Effect": "Deny"
    }
  ]
}"""

    create_tag = [
        {"Key": "org", "Value": "idem"},
        {"Key": "env", "Value": "test"},
        {"Key": "state", "Value": "temp"},
    ]

    create_policy_ret = await hub.states.aws.organizations.policy.present(
        ctx,
        name=policy_name,
        policy_name=policy_name,
        description=description,
        policy_type=policy_type,
        content=content,
        tags=create_tag,
    )

    assert create_policy_ret
    assert "Created aws.organizations.policy" in create_policy_ret["comment"]

    if not hub.tool.utils.is_running_localstack(ctx):
        assert dict(create_tag) == dict(
            create_policy_ret.get("new_state").get("Tags")
        ), "create_tag should equal the tag in new_state"

    assert description == create_policy_ret.get("new_state").get(
        "Description"
    ), "description should equal to the description in new_state"

    assert policy_name == create_policy_ret.get("new_state").get(
        "Name"
    ), "policy_name should equal the name in new_state"

    assert policy_type == create_policy_ret.get("new_state").get(
        "Type"
    ), "policy_type should equal to the Type in new_state"

    assert content == create_policy_ret.get("new_state").get(
        "Content"
    ), "content should equal the Content in new_state"

    updated_description = "idem_test_aws_update_ " + description
    update_tags = [
        {"Key": "org", "Value": "update_idem"},
        {"Key": "env", "Value": "update_test"},
    ]

    update_policy_ret = await hub.states.aws.organizations.policy.present(
        ctx,
        name=create_policy_ret.get("new_state")["Id"],
        policy_name=policy_name,
        description=updated_description,
        content=content,
        tags=update_tags,
    )

    assert update_policy_ret
    assert "Updated aws.organizations.policy" in update_policy_ret["comment"]

    assert update_policy_ret.get("old_state").get("Id") == create_policy_ret.get(
        "new_state"
    ).get("Id"), "New State of create ID should be equal to Old state of update ID"

    assert update_policy_ret.get("old_state").get("Arn") == create_policy_ret.get(
        "new_state"
    ).get("Arn"), "New State of create Arn should be equal to Old state of update Arn"

    assert update_policy_ret.get("old_state").get("Name") == create_policy_ret.get(
        "new_state"
    ).get(
        "Name"
    ), "New State of create Name should be equal to Old state of update Name"

    assert update_policy_ret.get("old_state").get(
        "Description"
    ) == create_policy_ret.get("new_state").get(
        "Description"
    ), "New State of create Description should be equal to Old state of update Description"

    assert update_policy_ret.get("old_state").get("Type") == create_policy_ret.get(
        "new_state"
    ).get(
        "Type"
    ), "New State of create Type should be equal to Old state of update Type"

    assert update_policy_ret.get("old_state").get(
        "AwsManaged"
    ) == create_policy_ret.get("new_state").get(
        "AwsManaged"
    ), "New State of create AwsManaged should be equal to Old state of update AwsManaged"

    assert update_policy_ret.get("old_state").get("Content") == create_policy_ret.get(
        "new_state"
    ).get(
        "Content"
    ), "New State of create Content should be equal to Old state of update Content"

    if "Tags" in update_policy_ret.get("old_state"):
        assert dict(update_policy_ret.get("old_state").get("Tags")) == dict(
            create_policy_ret.get("new_state").get("Tags")
        ), "New State of create Tags should be equal to Old state of update Tags"

    assert updated_description == update_policy_ret.get("new_state").get(
        "Description"
    ), "description should equal to the updated description in new_state"

    assert policy_name == update_policy_ret.get("new_state").get(
        "Name"
    ), "Policy Name should not change as there was no update to policy name"

    if not hub.tool.utils.is_running_localstack(ctx):
        assert dict(update_policy_ret.get("new_state").get("Tags")) == dict(update_tags)

    describe_ret = await hub.states.aws.organizations.policy.describe(ctx)

    assert update_policy_ret.get("new_state")["Id"] in describe_ret
    describe_params = describe_ret[update_policy_ret.get("new_state")["Id"]].get(
        "aws.organizations.policy.present"
    )
    assert update_policy_ret.get("new_state")["Name"] == describe_params[0].get(
        "policy_name"
    )
    assert update_policy_ret.get("new_state")["Description"] == describe_params[1].get(
        "description"
    )
    assert update_policy_ret.get("new_state")["Type"] == describe_params[2].get(
        "policy_type"
    )
    assert update_policy_ret.get("new_state")["Content"] == describe_params[3].get(
        "content"
    )

    if not hub.tool.utils.is_running_localstack(ctx):
        assert dict(update_policy_ret.get("new_state")["Tags"]) == dict(
            describe_params[4].get("tags")
        )

    delete_policy_ret = await hub.states.aws.organizations.policy.absent(
        ctx, name=update_policy_ret.get("new_state").get("Id")
    )

    assert delete_policy_ret["result"]

    assert (
        f"aws.organizations.policy {update_policy_ret.get('new_state').get('Id')} deleted."
        == delete_policy_ret["comment"]
    )

    delete_policy_ret_again = await hub.states.aws.organizations.policy.absent(
        ctx, name=update_policy_ret.get("new_state").get("Id")
    )

    assert (
        f"aws.organizations.policy {update_policy_ret.get('new_state').get('Id')} already absent"
        == delete_policy_ret_again["comment"]
    )
