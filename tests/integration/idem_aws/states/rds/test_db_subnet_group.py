import uuid

import pytest


# TODO IT tests against a AWS endpoint
@pytest.mark.skip(
    reason="This test only works with AWS endpoint. Localstack pro is not able to create EC2 resources"
)
@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_db_subnet_group(hub, ctx, aws_ec2_subnet):
    # Create DB Subnet Group
    db_subnet_group_name = "idem-test-db-subnet-group-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": db_subnet_group_name},
    ]
    ret = await hub.states.aws.rds.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        db_subnet_group_description="For testing idem plugin",
        subnets=[aws_ec2_subnet.get("SubnetId")],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created '{db_subnet_group_name}'" in ret["comment"]
    resource = ret["new_state"]
    ret_db_subnet_group_name = resource.get("DBSubnetGroupName")

    # Describe instance
    describe_ret = await hub.states.aws.rds.db_subnet_group.describe(ctx)
    assert ret_db_subnet_group_name in describe_ret

    # no changes to tags
    ret = await hub.states.aws.rds.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        db_subnet_group_description="For testing idem plugin",
        subnets=[aws_ec2_subnet.get("SubnetId")],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"'{db_subnet_group_name}' already exists" in ret["comment"]

    # Updating tags
    new_tags = [
        {"Key": "Name", "Value": "Updated"},
    ]

    ret = await hub.states.aws.rds.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        db_subnet_group_description="For testing idem plugin",
        subnets=[aws_ec2_subnet.get("SubnetId")],
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["new_state"]["tags"] == new_tags
    assert f"Updated '{db_subnet_group_name}'" in ret["comment"]

    # Describe instance
    describe_ret = await hub.states.aws.rds.db_subnet_group.describe(ctx)
    assert ret_db_subnet_group_name in describe_ret
    assert describe_ret.get(ret_db_subnet_group_name) and describe_ret.get(
        ret_db_subnet_group_name
    ).get("aws.rds.db_subnet_group.present")
    resource = describe_ret.get(ret_db_subnet_group_name).get(
        "aws.rds.db_subnet_group.present"
    )
    describe_tags = None
    for item in resource:
        if "tags" in item:
            describe_tags = item.get("tags")

    assert describe_tags == new_tags

    # removing tags
    ret = await hub.states.aws.rds.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        db_subnet_group_description="For testing idem plugin",
        subnets=[aws_ec2_subnet.get("SubnetId")],
        tags=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["old_state"]["tags"] == new_tags
    assert ret["new_state"]["tags"] == []
    assert f"Updated '{db_subnet_group_name}'" in ret["comment"]

    # Delete instance
    ret = await hub.states.aws.rds.db_subnet_group.absent(
        ctx, name=ret_db_subnet_group_name
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted '{db_subnet_group_name}'" in ret["comment"]

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.rds.db_subnet_group.absent(
        ctx, name=ret_db_subnet_group_name
    )
    assert ret["comment"] == f"'{db_subnet_group_name}' already absent"
