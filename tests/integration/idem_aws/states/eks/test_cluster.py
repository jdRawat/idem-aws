import uuid

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_eks_cluster(
    hub,
    ctx,
    aws_iam_cluster_role_assignment,
    aws_ec2_subnet_for_eks_worker,
    aws_ec2_subnet_for_eks,
    aws_security_group,
):
    if hub.tool.utils.is_running_localstack(ctx):
        return

    arn = aws_iam_cluster_role_assignment["Arn"]
    cluster_name = "idem-test-cluster-" + str(uuid.uuid4())
    ret = await hub.states.aws.eks.cluster.present(
        ctx,
        name=cluster_name,
        role_arn=arn,
        version="1.20",
        resources_vpc_config={
            "endpointPrivateAccess": True,
            "endpointPublicAccess": False,
            "subnetIds": [
                aws_ec2_subnet_for_eks_worker.get("resource_id"),
                aws_ec2_subnet_for_eks.get("resource_id"),
            ],
            "securityGroupIds": [aws_security_group.get("GroupId")],
        },
        kubernetes_network_config={
            "ipFamily": "ipv4",
            "serviceIpv4Cidr": "172.20.0.0/16",
        },
        tags={"Name": cluster_name},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == f"Created '{cluster_name}'"
    resource = ret.get("new_state")
    resource_id = resource.get("name")

    # Describe cluster
    describe_ret = await hub.states.aws.eks.cluster.describe(ctx)
    assert resource_id in describe_ret

    # same inputs as creation time, resource already exists
    ret = await hub.states.aws.eks.cluster.present(
        ctx,
        name=cluster_name,
        role_arn=arn,
        version="1.20",
        resources_vpc_config={
            "endpointPrivateAccess": True,
            "endpointPublicAccess": False,
            "subnetIds": [
                aws_ec2_subnet_for_eks_worker.get("resource_id"),
                aws_ec2_subnet_for_eks.get("resource_id"),
            ],
            "securityGroupIds": [aws_security_group.get("GroupId")],
        },
        tags={"Name": cluster_name},
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == f"'{resource_id}' already exists"

    # updating cluster configuration
    logging = {
        "clusterLogging": [
            {"enabled": True, "types": ["api", "scheduler"]},
            {
                "enabled": False,
                "types": ["audit", "authenticator", "controllerManager"],
            },
        ]
    }

    ret = await hub.states.aws.eks.cluster.present(
        ctx,
        name=cluster_name,
        role_arn=arn,
        version="1.20",
        resources_vpc_config={
            "endpointPrivateAccess": True,
            "endpointPublicAccess": False,
            "subnetIds": [
                aws_ec2_subnet_for_eks_worker.get("resource_id"),
                aws_ec2_subnet_for_eks.get("resource_id"),
            ],
            "securityGroupIds": [aws_security_group.get("GroupId")],
        },
        kubernetes_network_config={
            "ipFamily": "ipv4",
            "serviceIpv4Cidr": "172.20.0.0/16",
        },
        logging=logging,
        tags={"Name": cluster_name},
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == f"Updated '{resource_id}'"

    # update version
    ret = await hub.states.aws.eks.cluster.present(
        ctx,
        name=cluster_name,
        role_arn=arn,
        version="1.21",
        resources_vpc_config={
            "endpointPrivateAccess": True,
            "endpointPublicAccess": False,
            "subnetIds": [
                aws_ec2_subnet_for_eks_worker.get("resource_id"),
                aws_ec2_subnet_for_eks.get("resource_id"),
            ],
            "securityGroupIds": [aws_security_group.get("GroupId")],
        },
        kubernetes_network_config={
            "ipFamily": "ipv4",
            "serviceIpv4Cidr": "172.20.0.0/16",
        },
        logging=logging,
        tags={"Name": cluster_name},
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == f"Updated '{resource_id}'"

    # Delete cluster
    ret = await hub.states.aws.eks.cluster.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == f"Deleted '{resource_id}'"

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.eks.cluster.absent(ctx, name=resource_id)
    assert ret["comment"] == f"'{resource_id}' already absent"
