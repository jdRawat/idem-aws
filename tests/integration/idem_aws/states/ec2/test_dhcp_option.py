import uuid

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_dhcp_option(hub, ctx):
    # Create dhcp_option
    name = "idem-test-dhcp-option-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": name},
    ]
    dhcp_configurations = [
        {"Key": "domain-name-servers", "Values": [{"Value": "10.2.5.1"}]}
    ]

    ret = await hub.states.aws.ec2.dhcp_option.present(
        ctx, name=name, dhcp_configurations=dhcp_configurations, vpc_id=None, tags=tags
    )
    created_dhcp_option_id = ret["new_state"]["DhcpOptionsId"]
    if not hub.tool.utils.is_running_localstack(ctx):
        assert ret["new_state"]["Tags"] == tags
    assert ret["new_state"]["DhcpConfigurations"] == dhcp_configurations

    # verify that created dhcp_option is present
    describe_ret = await hub.states.aws.ec2.dhcp_option.describe(ctx)
    assert created_dhcp_option_id in describe_ret
    assert describe_ret.get(created_dhcp_option_id) and describe_ret.get(
        created_dhcp_option_id
    ).get("aws.ec2.dhcp_option.present")

    # Delete instance
    ret = await hub.states.aws.ec2.dhcp_option.absent(ctx, name=created_dhcp_option_id)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # Deleting the same instance again (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.ec2.dhcp_option.absent(ctx, name=created_dhcp_option_id)
    assert f"'{created_dhcp_option_id}' already absent" in ret["comment"]
