import copy
import os
import random
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_vpc(hub, ctx):
    vpc_temp_name = "idem-test-vpc-" + str(uuid.uuid4())
    cidr_block = os.getenv("IT_TEST_EC2_VPC_CIDR_BLOCK")
    num: () = lambda: random.randint(0, 255)
    if cidr_block is None:
        cidr_block = f"192.168.{num()}.0/24"
    cidr_block_association_set = [{"CidrBlock": cidr_block}]
    tags = [{"Key": "Name", "Value": vpc_temp_name}]

    # Create vpc with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.vpc.present(
        test_ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        instance_tenancy="default",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert "Would create aws.ec2.vpc" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert cidr_block == resource.get("cidr_block_association_set")[0].get("CidrBlock")
    assert tags == resource.get("tags")
    assert "default" == resource.get("instance_tenancy")
    assert vpc_temp_name == resource.get("name")

    # Create vpc
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        instance_tenancy="default",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert cidr_block == resource.get("cidr_block_association_set")[0].get("CidrBlock")
    assert tags == resource.get("tags")
    assert "default" == resource.get("instance_tenancy")
    assert vpc_temp_name == resource.get("name")
    resource_id = resource.get("resource_id")

    # Describe vpc
    describe_ret = await hub.states.aws.ec2.vpc.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.ec2.vpc.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.ec2.vpc.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert "cidr_block_association_set" in described_resource_map
    assert cidr_block == described_resource_map["cidr_block_association_set"][0].get(
        "CidrBlock"
    )
    assert "default" == described_resource_map.get("instance_tenancy")
    assert tags == described_resource_map.get("tags")

    # Test associating cidr block and adding tags
    cidr_block_2 = os.getenv("IT_TEST_EC2_VPC_CIDR_BLOCK_2")
    if cidr_block_2 is None:
        cidr_block_2 = f"192.168.{num()}.0/24"
    cidr_block_association_set.append({"CidrBlock": cidr_block_2})
    tags.append(
        {
            "Key": f"idem-test-vpc-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-vpc-value-{str(uuid.uuid4())}",
        }
    )
    # Update vpc with test flag
    ret = await hub.states.aws.ec2.vpc.present(
        test_ctx,
        name=vpc_temp_name,
        resource_id=resource_id,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    real_cidr_blocks = [
        cidr.get("CidrBlock") for cidr in resource.get("cidr_block_association_set")
    ]
    assert 2 == len(real_cidr_blocks)
    assert cidr_block in real_cidr_blocks
    assert cidr_block_2 in real_cidr_blocks
    assert tags == resource.get("tags")

    # Update vpc in real
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        resource_id=resource_id,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    real_cidr_blocks = [
        cidr.get("CidrBlock") for cidr in resource.get("cidr_block_association_set")
    ]
    assert 2 == len(real_cidr_blocks)
    assert cidr_block in real_cidr_blocks
    assert cidr_block_2 in real_cidr_blocks
    real_vpc_tags = resource.get("tags")
    assert real_vpc_tags
    assert 2 == len(real_vpc_tags)
    assert tags[0] in real_vpc_tags
    assert tags[1] in real_vpc_tags

    # Test disassociating cidr and deleting tags
    tags = [tags[0]]
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        resource_id=resource_id,
        cidr_block_association_set=copy.deepcopy([cidr_block_association_set[0]]),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert 1 == len(resource.get("cidr_block_association_set"))
    assert cidr_block_association_set[0].get("CidrBlock") == resource.get(
        "cidr_block_association_set"
    )[0].get("CidrBlock")
    assert tags == resource.get("tags")

    # Delete vpc with test flag
    ret = await hub.states.aws.ec2.vpc.absent(
        test_ctx, name=vpc_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert "Would delete aws.ec2.vpc" in ret["comment"]

    # Delete vpc
    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=vpc_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Deleting vpc again should be an no-op
    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=vpc_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert "already absent" in ret["comment"]
