import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_transit_gateway(hub, ctx):
    # Create transit_gateway
    transit_gateway_temp_name = "idem-test-transit-gateway-" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": transit_gateway_temp_name}]
    description = "my new transit gateway"
    ret = await hub.states.aws.ec2.transit_gateway.present(
        ctx, name=transit_gateway_temp_name, description=description, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"]["new"]
    resource = ret["changes"]["new"]
    assert tags == resource.get("Tags")
    assert description == resource.get("Description")
    resource_id = resource.get("TransitGatewayId")

    # Testing adding cidr blocks and tags
    options = resource.get("Options")
    options["TransitGatewayCidrBlocks"] = ["10.0.0.0/24"]
    tags.append(
        {
            "Key": f"idem-test-transit-gateway-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-transit-gateway-value-{str(uuid.uuid4())}",
        }
    )
    ret = await hub.states.aws.ec2.transit_gateway.present(
        ctx, name=resource_id, description=description, tags=tags, options=options
    )
    assert ret["result"], ret["comment"]
    assert ret["changes"].get("old") and ret["changes"].get("new")
    resource = ret["changes"].get("new")
    assert tags == resource.get("Tags")
    # "TransitGatewayCidrBlocks" doesn't work in localstack

    # Describe transit_gateway
    describe_ret = await hub.states.aws.ec2.transit_gateway.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.ec2.transit_gateway.present"
    )
    resource = describe_ret.get(resource_id).get("aws.ec2.transit_gateway.present")
    describe_tags = None
    for item in resource:
        if "tags" in item:
            describe_tags = item.get("tags")

    assert describe_tags == tags

    # Delete transit_gateway
    ret = await hub.states.aws.ec2.transit_gateway.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret["changes"]["old"] and not ret["changes"].get("new")

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.ec2.transit_gateway.absent(ctx, name=resource_id)
    assert ret["comment"] == f"'{resource_id}' already absent"
