import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_transit_gateway_vpc_attachment(
    hub, ctx, aws_ec2_vpc, aws_ec2_subnet, aws_ec2_transit_gateway
):
    # Create transit_gateway vpc attachment
    transit_gateway_vpc_attachment_temp_name = (
        "idem-test-transit-gateway-vpc-attachment" + str(uuid.uuid4())
    )
    tags = [{"Key": "Name", "Value": transit_gateway_vpc_attachment_temp_name}]
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.present(
        ctx,
        name=transit_gateway_vpc_attachment_temp_name,
        transit_gateway=aws_ec2_transit_gateway.get("TransitGatewayId"),
        vpc_id=aws_ec2_vpc.get("VpcId"),
        subnet_ids=[aws_ec2_subnet.get("SubnetId")],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert tags == resource.get("Tags")
    resource_id = resource.get("TransitGatewayAttachmentId")
    # Testing adding tags and updating options
    options = resource.get("Options")
    options["DnsSupport"] = "disable"
    tags.append(
        {
            "Key": f"idem-test-transit-gateway-vpc-attachment-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-transit-gateway-vpc-attachment-value-{str(uuid.uuid4())}",
        }
    )
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.present(
        ctx,
        name=resource_id,
        transit_gateway=aws_ec2_transit_gateway.get("TransitGatewayId"),
        vpc_id=aws_ec2_vpc.get("VpcId"),
        subnet_ids=[aws_ec2_subnet.get("SubnetId")],
        tags=tags,
        options=options,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert tags == resource.get("Tags")
    assert options == resource.get("Options")

    # Describe transit_gateway_vpc_attachment
    describe_ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.ec2.transit_gateway_vpc_attachment.present"
    )
    resource = describe_ret.get(resource_id).get(
        "aws.ec2.transit_gateway_vpc_attachment.present"
    )
    describe_tags = None
    for item in resource:
        if "tags" in item:
            describe_tags = item.get("tags")

    assert describe_tags == tags

    # Delete transit_gateway_vpc_attachment
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.absent(
        ctx, name=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.ec2.transit_gateway_vpc_attachment.absent(
        ctx, name=resource_id
    )
    assert ret["comment"] == f"'{resource_id}' already absent"
