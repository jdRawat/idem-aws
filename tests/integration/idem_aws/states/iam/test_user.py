import uuid

import pytest


@pytest.mark.asyncio
async def test_user(hub, ctx):
    # Create IAM user
    user_temp_name = "idem-test-user-" + str(uuid.uuid4())
    ret = await hub.states.aws.iam.user.present(
        ctx,
        name=user_temp_name,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    resource_id = resource.get("UserName")

    # Describe IAM User
    describe_ret = await hub.states.aws.iam.user.describe(ctx)
    assert resource_id in describe_ret

    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.iam.user.present"], "user_name", user_temp_name
    )

    # Delete IAM user
    ret = await hub.states.aws.iam.user.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # Delete IAM user again
    ret = await hub.states.aws.iam.user.absent(ctx, name=resource_id)
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and not ret["new_state"]
