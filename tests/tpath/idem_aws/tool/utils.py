import ipaddress
import random


def get_sub_cidr_block(hub, cidr_block: str, sub_mask: int):
    ips = ipaddress.IPv4Network(cidr_block)
    if ips.prefixlen < sub_mask < 32:
        sub_block_len = 2 ** (32 - sub_mask)
        new_start_ip_index = (
            random.randint(0, 2 ** (sub_mask - ips.prefixlen) - 1) * sub_block_len
        )
        new_start_ip = ips[new_start_ip_index]
        return f"{new_start_ip}/{sub_mask}"
    else:
        raise ValueError(
            f"Subnet mask {sub_mask} is not in the valid range between {ips.prefixlen} and 32."
        )


def verify_in_list(hub, items, key, value):
    # Verify a key value pair dictionary exists within a list
    found = False
    for item in items:
        if item.get(key, None):
            assert value == item.get(key), "Comparing " + key
            found = True
    assert found, "expecting " + key


def is_running_localstack(hub, ctx):
    return ctx.get("acct").get("aws_access_key_id") == "localstack"
