{%  for i in range(10) %}
idem_aws_demo_user_{{i}}:
  aws.iam.user.absent
{%  endfor -%}

my_role:
  aws.iam.role.absent

Vpcs[?Tags[?Key=='idem_demo_key']|[?Value=='my_vpc']].VpcId|[0]:
  aws.ec2.vpc.absent

Subnets[?Tags[?Key=='idem_demo_key']|[?Value=='my_subnet']].SubnetId|[-1]:
  aws.ec2.subnet.absent

Reservations[].Instances[]|[?Tags[?Key=='idem_demo_key']|[?Value=='my_instance']].InstanceId:
  aws.ec2.instance.absent
